import csv
import pathlib
from os import getcwd, sep


def get_path(file_name):
    """
    Auxiliary function to resolve absolute file paths and return the correct path to scraper/files/filename string

    :param file_name: str filename to be saved
    :return: adjusted file path
    """
    target_path = ['scraper', 'files', file_name]
    script_path = getcwd().split(sep)
    for i in range(len(script_path)):
        # Look up for the scraper directory so we can point to the files folder
        lookup_path = sep.join(script_path[:-i] + target_path[0:1])
        if pathlib.Path(lookup_path).is_dir():
            return sep.join(script_path + target_path)


def dump_to_file(product_list, file_name, encoding='UTF-8'):
    """
    Function to dump the contents of a list of named tuples to a csv file

    :param product_list: list of Product tuples
    :param file_name: file path and name to be saved
    :param encoding: str encoding to be used when opening the file
    :return:
    """
    with open(get_path(f'{file_name}.csv'), 'w', newline='', encoding=encoding) as csv_file:
        writer = csv.writer(csv_file, delimiter=';')
        writer.writerow(["URL", "TITLE", "NAME"])
        for product in product_list:
            writer.writerow(product)
