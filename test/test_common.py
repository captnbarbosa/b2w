import unittest
from test import BaseTestCase
from bs4 import BeautifulSoup
from scraper.epocacosmeticos.parser import load


class TestCommon(BaseTestCase):
    """
    Functional tests for scraper.epocacosmeticos module
    """
    def test_dump_to_file(self):
        """
        Functional test for scraper.common.csv_dump.dump_to_file
        """
        from random import sample
        from scraper.common.csv_dump import dump_to_file, get_path
        from scraper import Product
        from os import getcwd
        import pathlib

        # Create a data structure identical to the one produced by the scraper
        # and dump it to a file
        available_chars = 'qazwsxedcrfvtgbyhnujmikolp0123456789'
        file_data = []
        for i in range(10):
            file_data.append(
                Product(
                    "".join(sample(available_chars, 32)),
                    "".join(sample(available_chars, 16)),
                    "".join(sample(available_chars, 8))
                )
            )
        dump_to_file(file_data, "teste")
        # Check if the file exists
        self.assertTrue(
            pathlib.Path.exists(pathlib.Path(get_path("teste.csv")))
        )

    def test_get_regex(self):
        """
        Functional test for scraper.common.regular_expression.get_re_match
        Tests if regex patterns and function parameters are working as intended
        """
        # Selection used throughout the test cases
        innerpage_selection = load("https://www.epocacosmeticos.com.br/perfumes")
        from scraper.common.regular_expression import get_re_match
        javascript_snippet = innerpage_selection.select_one("div.resultItemsWrapper script").contents[0]
        # Test page count and group name
        page_count = get_re_match("pagecount_\d* = (?P<page_count>\d*);", javascript_snippet, default=0)
        self.assertLess(0, int(page_count))
        # Dummy default test
        dummy_string = "This is not javascript code."
        page_count = get_re_match("pagecount_\d* = (?P<page_count>\d*);", dummy_string, default=0)
        self.assertEqual(0, int(page_count))
