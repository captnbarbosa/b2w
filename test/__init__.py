import unittest
from unittest import TestCase


class BaseTestCase(TestCase):
    """
    Base class to set up tests.
    """
    def setUp(self):
        pass

    def tearDown(self):
        pass
