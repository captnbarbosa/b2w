### Instalação
A instalação é simples, basta seguir os passos abaixo

Clonar o repositório:

    git clone https://captnbarbosa@bitbucket.org/captnbarbosa/b2w.git

Instalar os pacotes

    pip install requirements.txt

E rodar o script bot.py a partir da raiz do projeto.

    python bot.py

O arquivo com os resultados será salvo na pasta scraper/files

### Respostas às questões propostas na especificação:

Agora você tem de capturar dados de outros 100 sites. Quais seriam suas estratégias para escalar a aplicação?

    Criaria uma classe para as funções no módulo scraper.epocacosmeticos
    para simplificar o acesso e padronizar a API para de acesso às funcionalidades
    de scraping. Isso não foi feito inicialmente por que a complexidade do 
    problema não demandava isso, e também por não ter informações sobre os demais 
    websites para saber como essa classe deveria ser estruturada 
    (sou adepto da simplicidade, sempre que possível).
    Havendo diversos outros sites, a padronização torna-se necessária.
    
    Usaria algo baseado no Celery (como o flower[1], por exemplo) para 
    gerenciar as instâncias de cada scraper e facilitar integração com 
    outros componentes (como bancos de dados, p.e.) e prover uma boa 
    interface para administração da aplicação;
    
    [1] https://flower.readthedocs.io/en/latest/

Alguns sites carregam o preço através de JavaScript. Como faria para capturar esse valor.
   
    Usaria um cliente HTTP como o ghost.py (meu favorito, por ser leve) 
    ou Selenium, com a opção para rodar sem GUI ('headless') de modo 
    reduzir o consumo de recursos do sistema.

Alguns sites podem bloquear a captura por interpretar seus acessos como um ataque DDOS. Como lidaria com essa situação?
   
    Supondo que haja consentimento do administrador do site para que o scraping 
    seja feito:
    - Entraria em contato para esclarecer a situação com o(s) administrador(es)
      do site em questão, e solicitar que o IP da aplicação conste uma whitelist. 
    - Caso isso não seja possível, tentaria usar proxies, IPs alternativos para 
      envio de requisições e/ou trabalhar com o espaço entre as requisições para 
      que sistemas de monitoramento não interpretem os acessos como ataques/ameaça.
   
Um cliente liga reclamando que está fazendo muitos acessos ao seu site e aumentando seus custos com infra. Como resolveria esse problema?
   
    A resposta abaixo considera apenas meu ponto de vista de como resolver o problema.
    Essas soluções seriam, claro, propostas e discutidas com pessoal da equipe para saber 
    se essa solução é adequada do ponto de vista do negócio.
    
    Depende da causa do aumento de custo:
    - Se for transferência de dados, procuraria recuperar os dados acessando apenas 
      o estritamente necessário (como acessar diretamente o JSON que a página carrega 
      ao invés da página propriamente dita, ou conjuntos maiores de produtos para reduzir 
      a quantidade de requisições etc);
    - Se for processamento: se for uma prática comum da empresa - tentaria estudar uma 
      alternativa junto ao cliente de cachear as informações necessárias para a prestação 
      do serviço. Talvez compense ainda (dependendo do caso, das características do contrato, 
      escopo do produto e das políticas da empresa etc) estudar alternativas para fazer esse 
      monitoramento, como por exemplo um arquivo consolidado para ser consumido periodicamente.
      Sempre há chance desse tipo de problema não ser algo isolado e, se não for, 
      uma alternativa como essa pode se tornar um diferencial para o produto.