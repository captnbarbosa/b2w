BASE_URL = "https://www.epocacosmeticos.com.br"

# List of categories that should not be allowed to enter the queue to be visited
BLACKLIST = (
    '/marcas',
    '/marcas#T',
    '/maybelline',
    '/carolina-herrera',
    '/paris-elysees',
    '/antonio-banderas',
    '/calvin-klein',
    '/maybelline',
    '/shiseido',
    '/dior',
    '/cerave',
    '/the-balm',
    '/clinique',
    '', '/','#',
)

WHITELIST = ('/marcas#Todas',)

PAGE_SELECTORS = ((None, "nav ul.menu__list li a"),  # Home page
                  #("https://www.epocacosmeticos.com.br/marcas#Todas", "ul.brand__list li.brand__item a")  # Brands page
                 )
