from scraper.concurrency import lock
from scraper.epocacosmeticos import PAGE_SELECTORS
from scraper.epocacosmeticos.parser import \
    load, \
    get_pagination_links, \
    parse_page, \
    parse_category_links


def worker(jobs, product_data, iterations=0):
    """
    Threaded function that parses a page for products and builds a list of Products named tuple to be inserted,
    and registers it on a dict using its URL as key

    :param jobs: queue.Queue shared queue of jobs
    :param product_data: list of Product named tuples
    :param iterations: AMount of times this worker should run. Default is 0 (run indefinitely)
    """
    i = 0
    while not jobs.empty():
        # Retrieve the URL to be parsed and load it,
        # and any pagination links in the page as well
        link = jobs.get()
        page_selection = load(link)
        pagination_links = get_pagination_links(page_selection)
        with lock:
            # Stores products not yet present in the product_data list
            products = parse_page(page_selection, (p.url for p in product_data))
            product_data += products
            # Adds any pagination url to the queue
            for pag_link in pagination_links:
                jobs.put(pag_link)
        jobs.task_done()
        # If a number of iterations was passed, will iterate only N times.
        # Setting iterations to 0 will make it run indefinitely
        i += 1
        if iterations == i:
            break


def load_queue(target_queue):
    """
    Auxiliary function to populate a queue with links to be visited by workers

    :param target_queue: queue.Queue to be filled
    """
    # Loads the home and brands pages using ((url, selector), (...)) pairs to retrieve category links
    # on different pages
    # Reminder: Passing None will cause the load() function to default to website home page
    for url, selector in PAGE_SELECTORS:
        page = load(url)
        for link in parse_category_links(page, selector):
            target_queue.put(link)
