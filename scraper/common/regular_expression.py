import re


def get_re_match(pattern, subject, default=None):
    """
    Auxiliary function to apply a regex pattern to a string and return the group(s) designated by group parameter.
    returns default is no value is found

    :param pattern: regex pattern
    :param subject: string to be searched
    :param default: default value to be returned if there are no matches
    :return: string result
    """
    p = re.compile(pattern)
    match = p.findall(subject)
    return default if not match else match[0]
