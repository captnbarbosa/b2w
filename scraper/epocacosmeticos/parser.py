import requests
from bs4 import BeautifulSoup
from bs4.element import NavigableString
from scraper import Product, Pagination
from scraper import get_re_match
from scraper.epocacosmeticos import BASE_URL, BLACKLIST, WHITELIST, PAGE_SELECTORS
from math import ceil


def load(target_url=None):
    """
    Function to load a specific URL, defaults to BASE_URL (website homepage)

    :param target_url: URL to be parsed
    :return: bs4.BeautifulSoup parsed document
    """
    response = requests.get(target_url or BASE_URL)
    return BeautifulSoup(response.content, features="html.parser")


def parse_category_links(selection, selector):
    """
    Function to retrieve links to product categories

    :return: list of URL
    """
    # Retrieve all links within the main menu, adding the website BASE_URL as needed.
    result = []
    # Iterate through category links and retrieves only the broader ones,
    # as to avoid unnecessary requests
    for list_item in selection.select(selector):
        if validate_category_link(list_item['href']):
            result.append(
                (BASE_URL if BASE_URL not in list_item['href'] else '') + list_item['href']
            )
    return result


def validate_category_link(_link):
    """
    Validates category links to avoid requests to subsets of broad categories

    :param _link: str Link to be validated
    :return: boolean
    """
    # Remove the URL
    link = _link.replace(BASE_URL, '')
    # From was seen in the website the target categories come right after the
    # base url, so we flag them as valid ones. Will also get the products by brand
    # since some products might not be tagged in those categories, but still have a brand
    if (link.count('/') > 1) or (link in BLACKLIST) or ('#' in link):
        return False
    return True


def get_pagination_links(selection):
    """
    Method to build a list of URL pages based on the page count and url found in the pagination javascript

    :param selection: bs4.BeautifulSoup selection
    :return: list of pagination URL to be parsed
    """
    # Retrieve the pagination info (number of pages and pagination link) from the script
    pagination_script = selection.select_one("div.resultItemsWrapper script")
    if pagination_script:
        # Here we extract page count and base URL and
        # change the pagination from the default 16 to 64 products / page
        pagination = get_pagination(pagination_script.contents[0])
        # Iterate through the amount of pages to generate the links.
        return [pagination.url+str(i)
                for i in range(1, pagination.page_count + 1)]
    return []


def get_pagination(javascript_snippet):
    """
    Auxiliary function parse the pagination page_count and URL from the javascript and return them in a namedtuple:
    Pagination(page_count, url), transforming page size to 64

    :param javascript_snippet: JS code to be searched
    :return: str pagination URL
    """
    # Retrieve the pagecount
    page_count = get_re_match(
        r"pagecount_\d* = (\d*);",
        javascript_snippet,
        default=0)
    # Retrieve the pagination url
    url = get_re_match(
        r"(\/buscapagina\S*PageNumber=)",
        javascript_snippet,
        default='')
    # Change pagesize from 16 to 64 and return the data
    return Pagination(int(ceil(int(page_count)/4)),
                      BASE_URL+url.replace("PS=16", "PS=64"))


def parse_page(selection, blacklist=()):
    """
    Function to parse a single page of products and return a list of Product named tuples

    :param selection: bs4.Beautifulsoup selection
    :param blacklist: tuple containing product URLs already in the dictionary
    :return: list of {url: Product(name, title, url)} named tuples
    """
    results = []
    for element in selection.select("div.shelf-default__item"):
        url = element.select_one("figure a")["href"]
        if url not in blacklist:
            # Create the product instance and append it to the results:
            # Product(Title, Product name, URL)
            results.append(
                Product(
                    element.select_one("a.brand").contents[0],
                    element.select_one("a.shelf-default__product-name").contents[0],
                    url,
                )
            )
    return results
