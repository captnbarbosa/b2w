import unittest
from unittest import TestCase
import queue
from scraper.concurrency.worker import worker, load_queue


class TestConcurrency(TestCase):
    """
    Functional tests for scraper.epocacosmeticos module
    """
    def test_load_queue(self):
        """
        Functional test for scraper.concurrency.worker.load_queue
        Tests if queue loading function works as intended
        """
        from scraper.concurrency.worker import load_queue
        test_queue = queue.Queue()
        load_queue(test_queue)
        self.assertLess(0, test_queue.qsize())


class TestWorker(TestCase):
    def setUp(self):
        """
        Setup method to the integration test:
         - List to hold the data retrieved from the worker and;
         - The job queue of URLs to be retrieved and parsed.
        """
        self.data = []
        # The LifoQueue here is because we are interested in the last inserted item
        self.test_queue = queue.LifoQueue()
        load_queue(self.test_queue)

    def test_worker(self):
        """
        Integration test for scraper.concurrency.worker.worker function
        Tests if the worker is appending pagination links to the queue and appending product data to the list
        """
        # We will take 2 measurements of the queue size: before and after a call to the worker is made.
        # The queue should be bigger, since pagination URLs would have been inserted
        q_size_0 = self.test_queue.qsize()
        # With 2 iterations we test:
        # - Pagination URLs retrieval from a category;
        # - Products from the first pagination URL.
        # As stated above, the LIFO queue will give us the pagination URL as soon as it is inserted in the queue.
        worker(self.test_queue, self.data, iterations=1)
        q_size_1 = self.test_queue.qsize()
        # The initial queue size should be smaller than the second one
        self.assertLess(q_size_0, q_size_1, msg="Checking queue size variation")
        # self.data should now hold products parsed from the page plus previous items
        self.assertLess(0, len(self.data), msg="Checking if product data was retrieved from the page")
