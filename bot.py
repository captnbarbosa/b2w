import threading
from scraper.concurrency.worker import worker, load_queue
from scraper.common.csv_dump import dump_to_file
import queue


N_THREADS = 3
# Product list to be dumped to a file
product_data = []
# Creation and load the jobs queue with URLs
jobs = queue.Queue()
load_queue(jobs)
threads = []

# Creation and starting of all threads
for t in range(N_THREADS):
    worker_thread = threading.Thread(target=worker, args=[jobs, product_data])
    threads.append(worker_thread)
    worker_thread.start()

# Blocks until all jobs are complete
jobs.join()

# Dumps file contents to the file
dump_to_file(product_data, "epocacosmeticos")
