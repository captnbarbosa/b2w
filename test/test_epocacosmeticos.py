import unittest
from test import BaseTestCase
from scraper.epocacosmeticos.parser import load


# Selection used throughout this suit of test cases
homepage_selection = load()
innerpage_selection = load("https://www.epocacosmeticos.com.br/perfumes")


class TestEpocacosmeticos(BaseTestCase):
    """
    Functional tests for scraper.epocacosmeticos module
    """
    def test_load(self):
        """
        Functional test for scraper.epocacosmeticos.load
        Tests if content loaded was correctly parsed by loading the website logo
        """
        self.assertTrue(homepage_selection.select_one("a.header__logo--link"))

    def test_parse_category_links(self):
        """
        Functional test for scraper.epocacosmeticos.parse_category_links
        Tests if categories were parsed and listed
        """
        from scraper.epocacosmeticos.parser import parse_category_links
        from scraper.epocacosmeticos import PAGE_SELECTORS
        # pass the selector used to retrieve products from the homepage: PAGE_SELECTORS[0][1]
        self.assertLess(0, len(parse_category_links(homepage_selection, PAGE_SELECTORS[0][1])))

    def test_get_pagination_links(self):
        """
        Functional test for scraper.epocacosmeticos.parse_pagination_links
        Tests if content loaded was correctly parsed by loading the website logo
        """
        from scraper.epocacosmeticos.parser import get_pagination_links
        self.assertLess(0, len(get_pagination_links(innerpage_selection)))

    def test_parse_page(self):
        """
        Functional test for scraper.epocacosmeticos.parse_page
        Tests if product titles, names and URLs ware parsed correctly from the homepage
        """
        from scraper.epocacosmeticos.parser import parse_page
        self.assertLess(0, len(parse_page(innerpage_selection)))

