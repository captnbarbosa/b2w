from collections import namedtuple


Pagination = namedtuple('Pagination', ('page_count', 'url'))
Product = namedtuple('Product', ('title', 'name', 'url'))
